var app = { util: {} }

app.util.requester = function (succ, fail) {
	$.ajax({
	  type: 'GET',
	  url: 'http://localhost:3000/user',
	  success: function (response) {
	  	if (succ)
	  		succ(response)
	  },
	  failure: function (response) {
	  	if (fail)
	  		fail(response)
	  }
	})
}

app.util.putter = function (data, succ, fail) {
	$.ajax({
	  type: 'PUT',
	  url: 'http://localhost:3000/user',
	  contentType: 'application/json',
	  data: JSON.stringify(data),
	  success: function (response) {
	  	if (succ)
	  		succ(response)
	  },
	  failure: function (response) {
	  	if (fail)
	  		fail(response)
	  }
	})
}


app.util.poster = function (id, data, succ, fail) {
	$.ajax({
	  type: 'POST',
	  url: 'http://localhost:3000/user/' + id,
	  contentType: 'application/json',
	  data: JSON.stringify(data),
	  success: function (response) {
	  	if (succ)
	  		succ(response)
	  },
	  failure: function (response) {
	  	if (fail)
	  		fail(response)
	  }
	})
}

app.util.deleter = function (id, succ, fail) {
	$.ajax({
	  type: 'DELETE',
	  url: 'http://localhost:3000/user/' + id,
	  success: function (response) {
	  	if (succ)
	  		succ(response)
	  },
	  failure: function (response) {
	  	if (fail)
	  		fail(response)
	  }
	})
}

app.perform = function () {
	var success = function (resp) {
		$('#response').append(resp + '</br>')
	}
	var failure = function (resp) {
		alert(resp)
	}
	for (var i = 0; i < 1/*0*/; i++) {
		app.util.requester(success, failure)
	}
	app.util.poster(12345678, { firstname: 'Jason', lastname: 'Boston' })
	app.util.putter({ firstname: 'Marie', lastname: 'Kreutz'})
	app.util.deleter(12345678)
}