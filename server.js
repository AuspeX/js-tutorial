var express = require('express')
var bodyParser = require('body-parser')
var app = express()

app.use(bodyParser.json())

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-M', '*')
  res.header('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

app.get('/user', function (req, res) {
	var usr = {
		id: 12345678,
		firstname: 'Jason',
		lastname: 'Bourne'
	}
	console.log('Got a GET request and returned data = ' + JSON.stringify(usr))
  res.send(JSON.stringify(usr))
})

// accept POST request at /user
app.post('/user/:id', function (req, res) {
	console.log('Got a POST request for \'' + req.params.id + '\' data = ' + JSON.stringify(req.body))
  res.send('Got a POST request')
})

// accept PUT request at /user
app.put('/user', function (req, res) {
	console.log('Got a PUT request data = ' + JSON.stringify(req.body))
  res.send('Got a PUT request')
})

// accept DELETE request at /user
app.delete('/user/:id', function (req, res) {	
	console.log('Trying to delete user with id \'' + req.params.id + '\'')
	res.send('Got a DELETE request')
})

var server = app.listen(3000, function () {
  var host = server.address().address
  var port = server.address().port
  console.log('Example app listening at http://%s:%s', host, port)
})